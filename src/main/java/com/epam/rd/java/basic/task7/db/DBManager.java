package com.epam.rd.java.basic.task7.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.epam.rd.java.basic.task7.db.entity.*;

public class DBManager {
    //private static final String DB_URL = "jdbc:mysql://localhost:3306/testdb?user=testuser&password=testpass";
    private static final String DB_URL = "jdbc:derby:memory:testdb;create=true";
    private static final DBManager instance = new DBManager();

    public static synchronized DBManager getInstance() {
        return instance;
    }

    private DBManager() {
    }

    public List<User> findAllUsers() throws DBException {
        String query = "SELECT * FROM users";

        try (Connection connection = DriverManager.getConnection(DB_URL);
             PreparedStatement statement = connection.prepareStatement(query)) {

            ResultSet resultSet = statement.executeQuery();

            List<User> users = new ArrayList<>();

            while (resultSet.next()) {
                User user = User.createUser(resultSet.getString(User.LOGIN_FIELD));
                user.setId(resultSet.getInt(User.ID_FIELD));

                users.add(user);
            }
            return users;

        } catch (SQLException e) {
            throw new DBException("Find all users error", e);
        }
    }

    public boolean insertUser(User user) throws DBException {
        String selectQuery = "SELECT * FROM users WHERE " + User.LOGIN_FIELD + "=?";
        String insertQuery = "INSERT INTO users VALUES (DEFAULT, ?)";

        try (Connection connection = DriverManager.getConnection(DB_URL);
             PreparedStatement selectStatement = connection.prepareStatement(selectQuery);
             PreparedStatement insertStatement = connection.prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS)) {

            selectStatement.setString(1, user.getLogin());
            ResultSet resultSet = selectStatement.executeQuery();

            if (resultSet.next()) {
                return false;
            }

            insertStatement.setString(1, user.getLogin());
            insertStatement.execute();
            resultSet = insertStatement.getGeneratedKeys();
            if (resultSet.next()) {
                user.setId(resultSet.getInt(1));
            }

            return true;
        } catch (SQLException e) {
            throw new DBException("Insert user error", e);
        }
    }

    public boolean deleteUsers(User... users) throws DBException {
        String query = "DELETE FROM users WHERE " + User.ID_FIELD + "=?";

        try (Connection connection = DriverManager.getConnection(DB_URL);
             PreparedStatement statement = connection.prepareStatement(query)) {
            for (User user : users) {
                statement.setInt(1, user.getId());
                statement.executeUpdate();
            }
        } catch (SQLException e) {
            throw new DBException("Delete user error", e);
        }
        return true;
    }

    public User getUser(String login) throws DBException {
        String query = "SELECT * FROM users WHERE " + User.LOGIN_FIELD + "=?";

        try (Connection connection = DriverManager.getConnection(DB_URL);
             PreparedStatement statement = connection.prepareStatement(query)) {

            statement.setString(1, login);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                User user = User.createUser(resultSet.getString(User.LOGIN_FIELD));
                user.setId(resultSet.getInt(User.ID_FIELD));
                return user;
            }
            throw new SQLException();
        } catch (SQLException e) {
            throw new DBException("Get user error", e);
        }
    }

    public Team getTeam(String name) throws DBException {
        String query = "SELECT * FROM teams WHERE " + Team.NAME_FIELD + "=?";

        try (Connection connection = DriverManager.getConnection(DB_URL);
             PreparedStatement statement = connection.prepareStatement(query)) {

            statement.setString(1, name);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                Team team = Team.createTeam(resultSet.getString(Team.NAME_FIELD));
                team.setId(resultSet.getInt(Team.ID_FIELD));
                return team;
            }
            throw new SQLException();
        } catch (SQLException e) {
            throw new DBException("Get team error", e);
        }
    }

    public List<Team> findAllTeams() throws DBException {
        String query = "SELECT * FROM teams";

        try (Connection connection = DriverManager.getConnection(DB_URL);
             PreparedStatement statement = connection.prepareStatement(query)) {

            ResultSet resultSet = statement.executeQuery();

            List<Team> teams = new ArrayList<>();

            while (resultSet.next()) {
                Team team = Team.createTeam(resultSet.getString(Team.NAME_FIELD));
                team.setId(resultSet.getInt(Team.ID_FIELD));

                teams.add(team);
            }
            return teams;

        } catch (SQLException e) {
            throw new DBException("Find all teams error", e);
        }
    }

    public boolean insertTeam(Team team) throws DBException {
        String selectQuery = "SELECT * FROM teams WHERE " + Team.NAME_FIELD + "=?";
        String insertQuery = "INSERT INTO teams VALUES (DEFAULT, ?)";

        try (Connection connection = DriverManager.getConnection(DB_URL);
             PreparedStatement selectStatement = connection.prepareStatement(selectQuery);
             PreparedStatement insertStatement = connection.prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS)) {

            selectStatement.setString(1, team.getName());
            ResultSet resultSet = selectStatement.executeQuery();

            if (resultSet.next()) {
                return false;
            }

            insertStatement.setString(1, team.getName());
            insertStatement.execute();
            resultSet = insertStatement.getGeneratedKeys();
            if (resultSet.next()) {
                team.setId(resultSet.getInt(1));
            }

            return true;
        } catch (SQLException e) {
            throw new DBException("Insert team error", e);
        }
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        String query = "INSERT INTO users_teams VALUES (?, ?)";

        Connection connection = null;
        try {
            connection = DriverManager.getConnection(DB_URL);

            try (PreparedStatement statement = connection.prepareStatement(query)){
                connection.setAutoCommit(false);

                for (Team team : teams) {
                    statement.setInt(1, user.getId());
                    statement.setInt(2, team.getId());

                    statement.executeUpdate();
                }
                connection.commit();
                connection.setAutoCommit(true);

                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            try {
                if (connection != null) {
                    connection.rollback();
                }
            } catch (SQLException ex) {
                e.printStackTrace();
                throw new DBException("Insert team rollback error", ex);
            }
            throw new DBException("Insert team error", e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
                throw new DBException("Insert team error", e);
            }
        }
    }

    public List<Team> getUserTeams(User user) throws DBException {
        String query = "SELECT * FROM teams JOIN users_teams ON teams.id = users_teams.team_id WHERE users_teams.user_id=?";

        try (Connection connection = DriverManager.getConnection(DB_URL);
             PreparedStatement statement = connection.prepareStatement(query)) {

            statement.setInt(1, user.getId());

            ResultSet resultSet = statement.executeQuery();

            List<Team> teams = new ArrayList<>();
            while (resultSet.next()) {
                Team team = Team.createTeam(resultSet.getString(Team.NAME_FIELD));
                team.setId(resultSet.getInt(Team.ID_FIELD));

                teams.add(team);
            }
            return teams;
        } catch (SQLException e) {
            throw new DBException("Get team error", e);
        }
    }

    public boolean deleteTeam(Team team) throws DBException {
        String query = "DELETE FROM teams WHERE " + Team.ID_FIELD + "=?";

        try (Connection connection = DriverManager.getConnection(DB_URL);
             PreparedStatement statement = connection.prepareStatement(query)) {

            statement.setInt(1, team.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DBException("Delete team error", e);
        }
        return true;
    }

    public boolean updateTeam(Team team) throws DBException {
        String query = "UPDATE teams SET " + Team.NAME_FIELD + "=? WHERE " + Team.ID_FIELD + "=?";

        try (Connection connection = DriverManager.getConnection(DB_URL);
             PreparedStatement statement = connection.prepareStatement(query)) {

            statement.setString(1, team.getName());
            statement.setInt(2, team.getId());

            return statement.execute();
        } catch (SQLException e) {
            throw new DBException("Update team error", e);
        }
    }
}
